import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje} from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { getAllLifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';
import { state } from '@angular/animations';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
      if (d != null) {
        this.updates.push('Se ha elegido ' + d.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items)
  }
  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d)
  }

  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }
getAll(){

}
}
